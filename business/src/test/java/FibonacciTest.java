package test.java;
import static org.junit.Assert.*;
import org.junit.Test;

import main.java.Fibonacci;

public class FibonacciTest {


	@Test
	public final void testFibonacciAssertTrue()
	{
		assertTrue(0== Fibonacci.fiboPrint(0));
		assertTrue(1== Fibonacci.fiboPrint(1));
		assertTrue(1== Fibonacci.fiboPrint(2));
		assertTrue(2== Fibonacci.fiboPrint(3));
		assertTrue(3== Fibonacci.fiboPrint(4));
		assertTrue(5== Fibonacci.fiboPrint(5));
		assertTrue(8== Fibonacci.fiboPrint(6));
		assertTrue(13== Fibonacci.fiboPrint(7));
		System.out.println("True");
	}


	@Test
	public final void testFibonacciAssertEquals() {
		assertEquals(1, Fibonacci.fiboPrint(1));
        assertEquals(1, Fibonacci.fiboPrint(1));
        assertEquals(1, Fibonacci.fiboPrint(2));
        assertEquals(2, Fibonacci.fiboPrint(3));
        assertEquals(3, Fibonacci.fiboPrint(4));
        assertEquals(5, Fibonacci.fiboPrint(5));
        System.out.println("Equals");
	}

}
