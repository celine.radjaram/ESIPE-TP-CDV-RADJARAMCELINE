package main.java;

import java.util.Scanner;
import org.apache.log4j.Logger;


public class Fibonacci {

	final static Logger logger = Logger.getLogger(Fibonacci.class);
	static int i = 0;

	public void run() {
		System.out.println("Please enter the number of terms you want from fibonacci series : "); 

		Scanner scanner = new Scanner(System.in);
		String read_tmp = scanner.nextLine();

		try {
			i = Integer.parseInt(read_tmp);
		} catch (Exception e) {
			i = 5;
		}
		
		for (int j = 1 ; j <= i ; j++ ) {
			System.out.print(fiboPrint(j)+"\n");
		}

	}
	public static int fiboPrint(int n){

		
			if (n == 0) return 0;
			else if (n == 1) return 1;
			else return fiboPrint(n - 1) + fiboPrint(n - 2);

	} 

	public void runLog(){

		if(logger.isInfoEnabled()){
			logger.info("This is info : " + i);
			fiboPrint(i);
		}
	}

}
